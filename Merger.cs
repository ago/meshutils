﻿using Assimp;

namespace FBXScaler
{
    internal class Merger
    {
        internal static Node? FindNode(Node rootNode, string name)
        {
            foreach (var node in rootNode.Children)
            {
                if (node.Name == name)
                    return node;

                if (node.HasChildren)
                {
                    var res = FindNode(node, name);
                    if (res != null)
                        return res;
                }
            }

            return null;
        }

        internal static bool ReplaceMesh(Scene sceneOld, Scene sceneNew, string name)
        {
            var fixedDistrictOld = FindNode(sceneOld.RootNode, name);
            if (fixedDistrictOld == null)
            {
                Console.WriteLine($"Unable to find area {name} in old mesh");
                return false;
            }

            var fixedDistrictNew = FindNode(sceneNew.RootNode, name);
            if (fixedDistrictNew == null)
            {
                Console.WriteLine($"Unable to find area {name} in new mesh");
                return false;
            }

            var meshIndices = fixedDistrictOld.MeshIndices.Zip(fixedDistrictNew.MeshIndices);
            foreach (var meshIndex in meshIndices)
            {
                sceneOld.Meshes[meshIndex.First] = sceneNew.Meshes[meshIndex.Second];
            }

            return true;
        }

        internal static void Merge(string inputFileOld, string inputFileNew, string outputFile)
        {
            var importer = new AssimpContext();
            var sceneOld = importer.ImportFile(inputFileOld, PostProcessPreset.TargetRealTimeMaximumQuality);
            var sceneNew = importer.ImportFile(inputFileNew, PostProcessPreset.TargetRealTimeMaximumQuality);

            ReplaceMesh(sceneOld, sceneNew, "_0106");
            ReplaceMesh(sceneOld, sceneNew, "_1502");

            //KBSAreaSorter.Sort(sceneOld);

            // Save the modified scene to a new DAE file
            importer.ExportFile(sceneOld, outputFile, "collada");
        }
    }
}
