﻿using Assimp;

namespace FBXScaler
{
    internal class KBSAreaSorter
    {
        internal static void Sort(Scene scene)
        {
            Console.WriteLine($"Sorting {scene.RootNode.Children.Count()} province(s)");
            var provinces = new List<Node>(scene.RootNode.Children);
            scene.RootNode.Children.Clear();
            scene.RootNode.Children.AddRange(provinces.OrderBy(p => p.Name).ToArray());

            foreach (var province in scene.RootNode.Children)
            {
                Console.WriteLine($"Processing province \"{province.Name}\"");

                Console.WriteLine($"Sorting {province.Children.Count()} district(s)");
                var districts = new List<Node>(province.Children);
                province.Children.Clear();
                province.Children.AddRange(districts.OrderBy(p => p.Name).ToArray());
            }
        }
    }
}
