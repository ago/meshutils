﻿using Assimp;
using System.Numerics;

namespace FBXScaler
{
    public static class MeshExtensions
    {
        public static Vector3D FindCenter(this Mesh mesh)
        {
            var center = new Vector3D(0, 0, 0);

            foreach (var vertex in mesh.Vertices)
            {
                center += vertex;
            }

            center /= mesh.VertexCount;

            return center;
        }

        public static void TranslateToOrigin(this Mesh mesh, Vector3D center)
        {
            for (int i = 0; i < mesh.VertexCount; i++)
            {
                mesh.Vertices[i] -= center;
            }
        }

        public static void Scale(this Mesh mesh, float scaleFactor)
        {
            for (int i = 0; i < mesh.VertexCount; i++)
            {
                mesh.Vertices[i] *= scaleFactor;
            }
        }

        public static void TranslateBack(this Mesh mesh, Vector3D center)
        {
            for (int i = 0; i < mesh.VertexCount; i++)
            {
                mesh.Vertices[i] += center;
            }
        }


        public static Vector3D CalculateFaceNormal(this Mesh mesh, Face face)
        {
            var normalSum = new Vector3D(0, 0, 0);
            foreach (int index in face.Indices)
            {
                normalSum += mesh.Normals[index];
            }

            var res = normalSum / face.Indices.Count;
            res.Normalize();
            return res;
        }

        public static Vector3D[] GetFaceUVCoordinates(this Mesh mesh, Face face)
        {
            var uvCoordinates = new Vector3D[face.IndexCount];

            for (int i = 0; i < face.IndexCount; i++)
            {
                var vertexIndex = face.Indices[i];
                // Assuming UV coordinates are stored in the first texture coordinate channel
                uvCoordinates[i] = mesh.TextureCoordinateChannels[0][vertexIndex];
            }

            return uvCoordinates;
        }
    }
}
