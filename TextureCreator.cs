﻿using Assimp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FBXScaler
{
    internal class TextureCreator
    {
        public delegate void NodeCallback(Node node);

        internal static void RecurseNodes(Node root, NodeCallback callback)
        {
            callback(root);
            foreach (var child in root.Children)
            {
                RecurseNodes(child, callback);
            }
        }

        internal static void Create(string inputFile, string texturePath)
        {
            var importer = new AssimpContext();
            var scene = importer.ImportFile(inputFile, PostProcessPreset.TargetRealTimeMaximumQuality);

            var fillBrush = Brushes.Black;
            var topBrush = Brushes.White;
            var topPen = new Pen(topBrush, 1.0f);
            var sideBrush = Brushes.Red;
            var sidePen = new Pen(sideBrush, 1.0f);

            RecurseNodes(scene.RootNode, delegate (Node node)
            {
                if (!node.Name.StartsWith('_'))
                    return;

                Console.WriteLine($"Processing node {node.Name}");

                var texture = new Bitmap(2048, 2048);

                using (var g = Graphics.FromImage(texture))
                {
                    //g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                    g.FillRectangle(Brushes.White, 0, 0, texture.Width, texture.Height);

                    foreach (var meshIdx in node.MeshIndices)
                    {
                        var mesh = scene.Meshes[meshIdx];

                        Console.WriteLine($"Processing mesh #{meshIdx}");

                        foreach (var face in mesh.Faces)
                        {
                            var faceNormal = mesh.CalculateFaceNormal(face);
                            var uv = mesh.GetFaceUVCoordinates(face);
                            var uvPoints = uv.Select(c => new PointF(c.X * texture.Width, (1.0f - c.Y) * texture.Height)).ToArray();

                            //Console.WriteLine($"{faceNormal.Y}");

                            //var normalColor = Color.FromArgb(
                            //    (int)(((faceNormal.X + 1.0f) / 2.0f) * 255.0f),
                            //    (int)(((faceNormal.Y + 1.0f) / 2.0f) * 255.0f),
                            //    (int)(((faceNormal.Z + 1.0f) / 2.0f) * 255.0f)
                            //    );
                            //var normalColor = Color.FromArgb(
                            //    0,
                            //    (int)(((faceNormal.Y + 1.0f) / 2.0f) * 255.0f),
                            //    0
                            //    );
                            //var normalBrush = new SolidBrush(normalColor);
                            //var normalPen = new Pen(normalBrush, 1.0f);

                            //g.FillPolygon(normalBrush, uvPoints);
                            //g.DrawPolygon(normalPen, uvPoints);

                            //continue;

                            //faceNormal.Y * 255.0f

                            if (faceNormal.Y >= 1)
                            {
                                /* Top */
                                //g.FillPolygon(topBrush, uvPoints);
                                //g.DrawPolygon(topPen, uvPoints);
                            }
                            else if (faceNormal.Y >= 0.5)
                            {
                                /* Bevel top */
                                //g.FillPolygon(Brushes.Orange, uvPoints);
                                //g.DrawPolygon(Pens.Orange, uvPoints);
                                g.FillPolygon(Brushes.Black, uvPoints);
                                g.DrawPolygon(Pens.Black, uvPoints);
                            }
                            else if (faceNormal.Y >= -0.5)
                            {
                                /* Sides */
                                //g.FillPolygon(Brushes.Green, uvPoints);
                                //g.DrawPolygon(Pens.Green, uvPoints);
                            }
                            else if (faceNormal.Y > -1.0)
                            {
                                /* Bevel bottom */
                                //g.FillPolygon(Brushes.Blue, uvPoints);
                                //g.DrawPolygon(Pens.Blue, uvPoints);
                            }
                            else
                            {
                                /* Bottom */
                                //g.FillPolygon(Brushes.Black, uvPoints);
                                //g.DrawPolygon(Pens.Black, uvPoints);
                            }
                        }

                        //foreach (var coord in mesh.TextureCoordinateChannels.First())
                        //{
                        //}
                    }
                }

                texture.Save(Path.Join(texturePath, $"{node.Name}.png"));
            });
        }
    }
}
