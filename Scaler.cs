﻿using Assimp;

namespace FBXScaler
{
    internal class Scaler
    {
        internal static void Scale(string inputFile, string outputFile, float scaleFactor = 1.1f)
        {
            var importer = new AssimpContext();
            var scene = importer.ImportFile(inputFile, PostProcessPreset.TargetRealTimeMaximumQuality);

            // Scale each mesh in the scene
            foreach (var mesh in scene.Meshes)
            {
                var center = mesh.FindCenter();
                mesh.TranslateToOrigin(center);
                mesh.Scale(scaleFactor);
                mesh.TranslateBack(center);
            }

            // Save the modified scene to a new DAE file
            importer.ExportFile(scene, outputFile, "collada");
        }
    }
}
